"""
@author : Roy Taylor, roy.taylor@siliconmechanics.com
Purpose: given a directory, will listen for the arrival of dumpfile
subdirectories. When a new dumpfile subdirectory appears, will update a
plot of the density (sliced to show Z-axis).
"""

from matplotlib import pyplot as plt
import numpy as np
import os
import yt
import time
import sys


def check_for_dumps(directory):
    """
    Every time it is invoked, will check for the dumps contained in the
    specified directory and will return a list of those dumps.
    """
    current_dir = os.getcwd()
    if directory == "." : target_dir = directory
    elif directory[0:5] != "/home" and sys.platform != 'darwin':
        target_dir = os.path.join("/home",directory)
    else: target_dir = directory
    ls = os.listdir(target_dir)
    list_of_dumps = []
    for article in ls:
        if article[0:2] == "DD":
            list_of_dumps.append(article)
    if len(list_of_dumps) == 0:
        list_of_dumps = []
    return list_of_dumps


def process_dump(dumpfile_location, data_dim=[30,30,90]):
    """
    Takes the dumpfile location*

    *two caveats:
        [1] do not include the dumpfile itself, e.g. "DD0001/" is okay
            but "DD0001/data0001" is not. THE SUBROUTINE EXPECTS the dump
            to be named 'dataXXXX' and appends accordingly.
        [2] path between exec and the dump cannot contain more than one
            instance of the string 'DD'.

    For example, if you want to process Dump 0012 and it's in the DD
    subdirectory, you'd call
    >>    dump_np = process_dump("DD0012")
    """
    dump_numeral = dumpfile_location[dumpfile_location.index("DD")+2:]
    directory = os.path.join(dumpfile_location, ("data"+dump_numeral))
    data = yt.load(directory)
    data = data.r[:]["density"]
    dump_np = data.ndarray_view().reshape(data_dim)
    return dump_np


def main(listen_directory):
    """
    Takes listen directory and SSH status as arguments.
    Runs real-time plotting.
    """
    continue_flag = False
    list_of_dumps = []
    while not continue_flag:
        list_of_dumps = check_for_dumps(listen_directory)
        if not list_of_dumps: pass
        else: continue_flag = True
    dumpfile_size = len(list_of_dumps)
    frm_1 = process_dump(os.path.join(listen_directory, list_of_dumps[-1]))
    frame_1 = frm_1[15,:,:].transpose()
    plt.set_cmap("viridis")
    plt.axis([0, 29, 0, 89]) #this will need to change with size
    plt.ion
    plt.contourf(-1*frame_1)
    plt.pause(.005)
    while True:
        list_of_dumps = check_for_dumps(listen_directory)
        list_of_dumps.sort()
        if len(list_of_dumps) > dumpfile_size:
            update_directory = os.path.join(listen_directory, list_of_dumps[-2])
            data = process_dump(update_directory)
            frame_data = data[15,:,:].transpose()
            plt.contourf(-1*frame_data)
            plt.title(list_of_dumps[-2])
            plt.pause(.005)
            dumpfile_size = len(list_of_dumps)
        else: plt.pause(.005)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        main(".")
    else:
        do_location = sys.argv[1]
        main(do_location)
    print "Done!"
